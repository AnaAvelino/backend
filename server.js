const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./app/models");
const Function = db.funcao;

const app = express();
app.use(cors());
var corsOptions = {
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.json({ msg: "Seja bem vindo(a) a Market Energy" });
});

const PORT = process.env.PORT || 8001;
app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
// console.log(app);
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Soltar e ressincronizar Db");
//   // initial();
// });

// function initial() {
//   Function.create({
//     id: 1,
//     name: "user",
//   });
// }
require("./app/routes/auth")(app);
require("./app/routes/user")(app);
require("./app/routes/providers")(app);
