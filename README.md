## Para executar o projeto, será necessário instalar os seguintes programas:

- [Node 14.16.1]

Para prosseguir com o desenvolvimento é necessario clonar o projeto do diretorio a sua escolha

cd "seu diretorio"
git clone https://AnaAvelino@bitbucket.org/AnaAvelino/backend.git


## Executando Comandos
no diretorio do projeto execute os seguintes comandos
 `npm install` instala os modulos listados do package.json
 `nodemon server.js` ou `node server.js`(caso opte em não usar nodemon)   inicia a aplicação

## Features
O projeto foi desenvolvindo com o intuido de exibir fornecedores de energia, para facilitar a busca feita por eventuais clientes