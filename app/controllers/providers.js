const db = require("../models");
const config = require("../config/auth");
const Providers = db.providers;

const Op = db.Sequelize.Op;

exports.selectProviders = (req, res) => {
  Providers.findAll({
    where: {
      limit_min_kwh: {
        [Op.gt]: req.params.limit,
      },
    },
  })
    .then((providers) => {
      var array = [];
      array.push(providers);
      console.log(array);

      res.status(200).send({
        providers,
      });
    })
    .catch((error) => {
      res
        .status(500)
        .send(
          { msg: "Nenhum resultado encontrado para sua busca" } +
            error +
            "essse"
        );
    });
};
