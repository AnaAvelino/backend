const db = require("../models");
const config = require("../config/auth");
const User = db.user;
const funcao = db.funcao;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  console.log("------------>", req.body.records);
  User.create({
    username: req.body.records.username,
    email: req.body.records.email,
    password: bcrypt.hashSync(req.body.records.password, 8),
  })
    .then((user) => {
      res.send({ msg: "Usuario cadastrado com sucesso!", tipo: 3 });
    })
    .catch((error) => {
      res.status(500).send({ msg: "vazio" });
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.credenciais.username,
    },
  })
    .then((user) => {
      if (!user) {
<<<<<<< HEAD
        return res.status(404).send({ msg: "Usuario não encontrado", tipo: 4 });
=======
        return res.status(404).send({ msg: "Usuario não existe", tipo: 4 });
>>>>>>> a9c7c4267fd6ee4f4fa60422fb8a2a89785069e3
      }
      var validaSenha = bcrypt.compareSync(
        req.body.credenciais.password,
        user.password
      );

      if (!validaSenha) {
        return res.status(401).send({
          acessToken: null,
          msg: "Atenção: Senha invalida!",
          tipo: 5,
        });
      }
      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400,
      });

      res.status(200).send({
        token: token,
      });

    })
    .catch((err) => {
      console.log("ERRO:" + err);
    });
};
