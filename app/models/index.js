const configs = require("../config/db");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(configs.DB, configs.USER, configs.PASSWORD, {
  host: configs.HOST,
  dialect: configs.dialect,
  operatorAliases: false,

  pool: {
    max: configs.pool.max,
    min: configs.pool.min,
    acquire: configs.pool.acquire,
    idle: configs.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user")(sequelize, Sequelize);
db.funcao = require("../models/funcao")(sequelize, Sequelize);
db.providers = require("../models/providers")(sequelize, Sequelize);


db.funcao.belongsToMany(db.user, {
  through: "user_funcao",
  foreignKey: "funcaoId",
  otherKey: "userId",
});

db.user.belongsToMany(db.funcao, {
  through: "user_funcao",
  foreignKey: "userId",
  otherKey: "funcaoId",
});

// db.providers.init(db.providers, {
//   through: "user_funcao",
//   foreignKey: "userId",
//   otherKey: "funcaoId",
// });

db.Functions = ["user", "admin", "moderator"];

module.exports = db;
