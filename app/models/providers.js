module.exports = (sequelize, Sequelize) => {
  const providers = sequelize.define("providers", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
    },
    logo: {
      type: Sequelize.STRING,
    },
    state: {
      type: Sequelize.STRING,
    },
    cost_kwh: {
      type: Sequelize.STRING,
    },
    limit_min_kwh: {
      type: Sequelize.STRING,
    },
    customers: {
      type: Sequelize.STRING,
    },
    assessment: {
      type: Sequelize.STRING,
    },
  });

  return providers;
};
