const authJwt = require("./authJwt");
const verificaCadastro = require("./verifySignUp");

module.exports = {
  authJwt,
  verificaCadastro,
};
