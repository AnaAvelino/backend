const db = require("../models");
const FUNCTIONS = db.Functions;
const User = db.user;

verificaNomeEmail = (req, res, next) => {
  User.findOne({
    where: {
      username: req.body.records.username,
    },
  }).then((user) => {
    if (user) {
      res.status(400).send({
        msg: "Atenção: Nome de usuario já está em uso",tipo: 1,
      });
      return;
    }

    User.findOne({
      where: {
        email: req.body.records.email,
      },
    }).then((user) => {
      if (user) {
        res.status(400).send({
          msg: "Atenção: Email já possui cadastro!",tipo:2,
        });

        return;
      }

    })
    // .then((user)=> {
    //   if(req.body.records.password != passwordconfirm ){
    //             res.status(400).send({
    //       msg: "Atenção: Email já possui cadastro!",tipo:2,
    //     });

    //     return;
    //   }
      
      next();
    // });
  });
};



const verificaCadastro = {
  verificaNomeEmail: verificaNomeEmail,
};

module.exports = verificaCadastro;
