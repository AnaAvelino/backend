const jwt = require("jsonwebtoken");
const config = require("../config/auth.js");
const db = require("../models");
const User = db.user;

verificacaoToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "Token não foi fornecido!",
    });
  }

  // if (user && user.accessToken) {
  //   // for Node.js Express back-end
  //   return { "x-access-token": user.accessToken };
  // }
  jwt.verify(Token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "não foi autorizado!",
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getFunctions().then((functions) => {
      for (let i = 0; i < functions.length; i++) {
        if (functions[i].name === "admin") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Admin Role!",
      });
      return;
    });
  });
};

isModerator = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getFunctions().then((functions) => {
      for (let i = 0; i < functions.length; i++) {
        if (functions[i].name === "moderator") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Moderator Role!",
      });
    });
  });
};

isModeratorOrAdmin = (req, res, next) => {
  User.findByPk(req.userId).then((user) => {
    user.getFunctions().then((functions) => {
      for (let i = 0; i < functions.length; i++) {
        if (functions[i].name === "moderator") {
          next();
          return;
        }

        if (functions[i].name === "admin") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "Require Moderator or Admin Role!",
      });
    });
  });
};

const authJwt = {
  verificacaoToken: verificacaoToken,
  isAdmin: isAdmin,
  isModerator: isModerator,
  isModeratorOrAdmin: isModeratorOrAdmin,
};
module.exports = authJwt;
